defmodule Issuer do
  @moduledoc """
  Documentation for `Issuer`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Issuer.hello()
      :world

  """
  def hello do
    :world
  end
end
